package prova.neppo.logica;

public class FormulaMisteriosa {

	private static Long formulaMisteriosa(Long entrada) {
		String vetor = Long.toString(entrada);
		String result = "";
		int atual;
		int prox;
		int sum;

		if (vetor.length() > 1) {
			atual = 0;
			prox = 1;
			sum = 1;

			while (atual <= vetor.length() - 1) {
				if (prox <= vetor.length() - 1 && vetor.charAt(atual) == vetor.charAt(prox)) {
					while (prox <= vetor.length() - 1 && vetor.charAt(atual) == vetor.charAt(prox)) {
						sum++;
						prox++;
					}
					result += String.valueOf(sum) + vetor.substring(atual, atual + 1);
					atual = prox;
					prox++;
					sum = 1;
				} else {
					result += 1 + vetor.substring(atual, atual + 1);
					atual++;
					prox++;
				}

			}
		} else {
			result = vetor + vetor;
		}

		return Long.parseLong(result);
	}

	public static void main(String[] args) {
		System.out.println("1 - " + formulaMisteriosa(1L));
		System.out.println("11 - " + formulaMisteriosa(11L));
		System.out.println("21 - " + formulaMisteriosa(21L));
		System.out.println("1211 - " + formulaMisteriosa(1211L));
		System.out.println("111221 - " + formulaMisteriosa(111221L));

	}

}