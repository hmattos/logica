package prova.neppo.logica;

public class GarconsModernos {

	private static int[] anos = { 1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1 };

	private static String[] anosRomanos = { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

	private static String toRomano(int ano) {
		String numeroRomano = "";

		for (int i = 0; i < anos.length; i++) {
			while (ano >= anos[i]) {
				numeroRomano += anosRomanos[i];
				ano -= anos[i];
			}
		}
		return numeroRomano;
	}

	public static void main(String[] args) {
		System.out.println(toRomano(4));
		System.out.println(toRomano(9));
		System.out.println(toRomano(40));
		System.out.println(toRomano(90));
		System.out.println(toRomano(400));
		System.out.println(toRomano(900));
		System.out.println(toRomano(1904));
		System.out.println(toRomano(1954));
		System.out.println(toRomano(1990));
		System.out.println(toRomano(2014));

	}

}