package prova.neppo.logica;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;

public class MaquinaDoTempo {
	
	private static boolean anoBissextos(int ano) {
		return ano%4 == 0 && ano%100 != 0 || ano%400 ==0;
	}
	
	private static boolean validaData(String data) {
		int dia;
		int mes;
		int ano;
		StringTokenizer st = new StringTokenizer(data);
		
		dia = Integer.parseInt(st.nextToken(" "));
		mes = Integer.parseInt(st.nextToken(" "));
		ano = Integer.parseInt(st.nextToken(" "));
		
		return validaAno(ano) && validaMes(mes) && validaDia(dia, mes, ano);
		
	}
	
	private static boolean validaMes(int mes) {
		if(mes > 0 && mes <= 12) {
			return true;
		}
		return false;
	}
	
	private static boolean validaDia(int dia, int mes, int ano) {
		List<Integer> listMeses31 = Arrays.asList(1, 3, 5, 7, 8, 10, 12);
		List<Integer> listMeses30 = Arrays.asList(4, 6, 9, 11);
		
		if(mes == 2 && dia > 0 && dia <= 29 && anoBissextos(ano)) {
			return true;
		}
		
		if(mes == 2 && dia > 0 && dia <= 28 && !anoBissextos(ano)) {
			return true;
		}
		
		if(listMeses31.contains(mes) && dia <= 31 && dia > 0) {
			return true;
		}
		
		if(listMeses30.contains(mes) && dia <= 30 && dia > 0) {
			return true;
		}
		
		return false;
	}
	
	private static boolean validaAno(int ano) {
		if(ano > 0) {
			return true;
		}
		
		return false;
	}
	
	public static void main(String[] args) {

		System.out.println(validaData("28 2 2017"));
		System.out.println(validaData("29 2 2017"));
		System.out.println(validaData("30 11 2017"));
		System.out.println(validaData("31 11 2017"));
		

	}

}
